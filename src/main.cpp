/*
 * LilyGo-HeartRate-Kit , Simple heart rate meter
 * main.cpp
 * Wirte by  Lewis he , 2020
 */
#include "painlessMesh.h"
#include <ArduinoJson.h>
#include <Wire.h>
#include <TFT_eSPI.h>
#include <pcf8563.h>
#include "esp_adc_cal.h"
#include "MAX30102_PulseOximeter.h"

#define DEBUG false

// Has been defined in the TFT_eSPI library
// #define TFT_RST                  26
// #define TFT_MISO                 -1
// #define TFT_MOSI                 19
// #define TFT_SCLK                 18
// #define TFT_CS                   5
// #define TFT_DC                   23
// #define TFT_BL                   27

#define I2C_SDA_PIN 21
#define I2C_SCL_PIN 22
#define RTC_INT_PIN 34
#define BATT_ADC_PIN 35
//#define VBUS_PIN 37
//#define LED_PIN 33
#define CHARGE_PIN 32
//#define BUTTON_PIN 38

#define HEATRATE_SDA 15
#define HEATRATE_SCL 13
#define HEATRATE_INT 4

// #define ENABLE_SOFT_AP //Uncomment this line will connect to the specified SSID

// #define WIFI_SSID "WIFI SSID"
// #define WIFI_PASSWD "WIFI PASSWD"

#define SCREEN_TIMEOUT 60000 //Enter deep sleep after timeout of 1 minute

TFT_eSPI tft = TFT_eSPI(); // Invoke library, pins defined in User_Setup.h
PCF8563_Class rtc;
PulseOximeter pox;

uint32_t targetTime = 0;
int vref = 1100;
bool charge_indication = false;
//bool update = false;
//bool touch_out = false;

/*MAX30105*/
const uint8_t RATE_SIZE = 4; //Increase this for more averaging. 4 is good.
uint8_t rates[RATE_SIZE];    //Array of heart rates
uint8_t rateSpot = 0;
int beatAvg;
bool find_max30105 = false;
bool showError = false;

// mesh network
#define MESH_PREFIX "whateverYouLike"
#define MESH_PASSWORD "somethingSneaky"
#define MESH_PORT 5555
#define ROLE "orange"
SimpleList<uint32_t> nodes;          // number of nodes connected 0 = no connection
uint32_t nsent = 0;                  // total messages sent, makes the client aware of missing messages
char CONNECTED[32] = "disconnected"; // this node is disconnected at startup
char CALLBACK[32] = "Name unbekannt"; // stores the extracted patient name
char TOTALMSG[6] = "0";
Scheduler userScheduler; // send spo2/hr every 10 sec
painlessMesh mesh;

void enterDeepsleep(void);
void showPatientName();
void showConnectionStatus();
extern const unsigned short heart[0x1000];

TFT_eSprite spr = TFT_eSprite(&tft);         // Declare Sprite object "spr" with pointer to "tft" object
TFT_eSprite stext1 = TFT_eSprite(&tft);      // Sprite object stext2
TFT_eSprite stext2 = TFT_eSprite(&tft);      // Sprite object stext2
TFT_eSprite spo2text = TFT_eSprite(&tft);    // Sprite object
TFT_eSprite patientname = TFT_eSprite(&tft); // Namwe des Patienten
TFT_eSprite hr = TFT_eSprite(&tft);          // HR
TFT_eSprite spo2 = TFT_eSprite(&tft);        // SPO2

void sendMessage()
{
    // send only when connected
    if (nodes.size() > 0)
    {
        DynamicJsonDocument jsonBuffer(1024);
        JsonObject msg = jsonBuffer.to<JsonObject>();
        //sprintf(msg["hr"], "%d", n_heart_rate);
        //sprintf(msg["spo2"], "%.0f", n_spo2);
        msg["nodeId"] = mesh.getNodeId();
        msg["role"] = ROLE;
        msg["hr"] = beatAvg;
        msg["spo2"] = pox.getSpO2();
        msg["msgno"] = nsent;
        msg["name"] = CALLBACK;
        String str;
        serializeJson(msg, str);
        mesh.sendBroadcast(str);
        Serial.printf("Tx-> %s\n", str.c_str()); // and to the serial console
        nsent++;                                 // increase total messages counter;
    }
}

Task taskSendMessage(TASK_SECOND * 10, TASK_FOREVER, &sendMessage);
// Empfangen von Nachrichten
void receivedCallback(uint32_t from, String &msg)
{
    Serial.printf("Rx from %u <- %s\n", from, msg.c_str());

    DynamicJsonDocument doc(1024);

    // Incoming message must be a JSON String
    // String input =  "{\"id\":1351824120,\"name\":\"Hans Meier\"}";
    deserializeJson(doc, msg);
    JsonObject obj = doc.as<JsonObject>();

    // read the node id from json and if it matches read the name
    uint32_t id = obj[String("id")];
    if (id == mesh.getNodeId())
    {
        // You can get a String from a JsonObject or JsonArray:
        String name = obj["name"];

        sprintf(CALLBACK, "%s", name.c_str());
        Serial.printf(CALLBACK);
        patientname.drawString(String(CALLBACK), 0, 0);
        showPatientName();
    }
}

void newConnectionCallback(uint32_t nodeId)
{
    Serial.printf("--> Start: New Connection, nodeId = %u\n", nodeId);
    Serial.printf("--> Start: New Connection, %s\n", mesh.subConnectionJson(true).c_str());
}

void showConnectionStatus()
{

    stext2.fillSprite(TFT_BLACK);
    stext2.setCursor(0, 0);
    stext2.print(CONNECTED);
    stext2.pushSprite(75, 5);
}
void showPatientName()
{
    patientname.fillSprite(TFT_BLACK);
    patientname.setCursor(0, 0);
    patientname.print(CALLBACK);
    patientname.pushSprite(75, 65);
}
void changedConnectionCallback()
{
    Serial.printf("Changed connections\n");

    nodes = mesh.getNodeList();
    Serial.printf("Num nodes: %d\n", nodes.size());
    Serial.printf("Connection list:");
    SimpleList<uint32_t>::iterator node = nodes.begin();
    while (node != nodes.end())
    {
        Serial.printf(" %u", *node);
        node++;
    }
    Serial.println();
    //calc_delay = true;
    if (nodes.size() > 0)
    {
        sprintf(CONNECTED, "con %d Nodes", nodes.size());
    }
    else
    {
        sprintf(CONNECTED, "Id:%u", mesh.getNodeId());
    }
    showConnectionStatus();
}

void nodeTimeAdjustedCallback(int32_t offset)
{
    Serial.printf("Adjusted time %u Offset = %d\n", mesh.getNodeTime(), offset);
}

void onNodeDelayReceived(uint32_t nodeId, int32_t delay)
{
    Serial.printf("Delay from node:%u delay = %d\n", nodeId, delay);
}

// Callback (registered below) fired when a pulse is detected
void onBeatDetected()
{

    rates[rateSpot++] = (byte)pox.getHeartRate(); //Store this reading in the array
    rateSpot %= RATE_SIZE;                    //Wrap variable

    //Take average of readings
    beatAvg = 0;
    for (byte x = 0; x < RATE_SIZE; x++)
        beatAvg += rates[x];
    beatAvg /= RATE_SIZE;
    if (beatAvg < 40) {beatAvg = 0;}
    // print the average of the heart rate
    if (DEBUG)
    {
        Serial.print("Heart rate:");
        Serial.print(beatAvg);
        Serial.print("bpm / SpO2:");
        Serial.print(pox.getSpO2());
        Serial.println("%");
    }
    //
    char HR[4] = "";
    sprintf(HR, "%d", beatAvg);
    stext1.fillSprite(TFT_BLACK);
    stext1.drawString(String(HR), 0, 0);
    stext1.pushSprite(75, 30);

    spo2text.fillSprite(TFT_BLACK);
    spo2text.drawString(String(pox.getSpO2()), 0, 0);
    spo2text.pushSprite(120, 30);

    hr.pushSprite(75, 18);
    spo2.pushSprite(120, 18);
}

bool setupMAX30105(void)
{

    Serial.print("Initializing pulse oximeter..");

    // Initialize the PulseOximeter instance
    pox.resume();
    if (!pox.begin())
    {
        Serial.println("FAILED");
        find_max30105 = false;
        return false;
    }
    else
    {
        Serial.println("SUCCESS");
    }

    // The default current for the IR LED is 50mA and it could be changed
    //   by uncommenting the following line. Check MAX30102_Registers.h for all the
    //   available options.
    pox.setIRLedCurrent(MAX30102_LED_CURR_24MA);

    // Register a callback for the beat detection
    pox.setOnBeatDetectedCallback(onBeatDetected);
    find_max30105 = true;
    return true;
}

void loopMAX30105(void)
{
    if (!find_max30105 && !showError)
    {
        tft.fillScreen(TFT_BLACK);
        tft.drawString("No sensor", 40, 30);
        showError = true;
        return;
    }

    if (showError)
    {
        return;
    }
    pox.update();
}

void setupMonitor()
{
    esp_adc_cal_characteristics_t adc_chars;

    if (esp_adc_cal_characterize((adc_unit_t)ADC_UNIT_1,
                                 (adc_atten_t)ADC1_CHANNEL_7,
                                 (adc_bits_width_t)ADC_WIDTH_BIT_12,
                                 1100,
                                 &adc_chars) == ESP_ADC_CAL_VAL_EFUSE_VREF)
    {
        vref = adc_chars.vref;
    }

    pinMode(CHARGE_PIN, INPUT);
    attachInterrupt(
        CHARGE_PIN, [] {
            charge_indication = true;
        },
        CHANGE);

    if (digitalRead(CHARGE_PIN) == LOW)
    {
        charge_indication = true;
    }
}

String getVoltage()
{
    uint16_t v = analogRead(BATT_ADC_PIN);
    float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);
    return String("Battery: ")+String(battery_voltage) + "V";
}

void setup(void)
{
    Serial.begin(115200);
    while (!Serial)
    {
        ;
    }
    // create the startup screen
    tft.init();
    tft.setRotation(3);

    //pinMode(LED_PIN, OUTPUT);
    Wire.begin(I2C_SDA_PIN, I2C_SCL_PIN);
    Wire1.begin(HEATRATE_SDA, HEATRATE_SCL); // MAX Library is internally adapeted to use Wire1

    tft.fillScreen(TFT_BLACK);
    tft.setTextFont(2);
    tft.setCursor(0, 0);
    tft.setTextColor(TFT_GREEN);

    if (!setupMAX30105())
    {
        tft.setTextColor(TFT_RED);
        tft.println("Check MAX30105 FAIL");
        tft.setTextColor(TFT_GREEN);
    }
    else
    {
        tft.println("Check MAX30105 PASS");
    }

    setupMonitor();
    tft.print("Correction Vref=");
    tft.print(vref);
    tft.println(" mv");
    tft.println(getVoltage());

    setCpuFrequencyMhz(80);
    // display the startup screen for 5 sec
    delay(5000);

    // create the heart rate display to show HR and SPO2 values
    // HR Headline
    hr.setColorDepth(8);
    hr.createSprite(32, 8);
    hr.fillSprite(TFT_BLACK);    // Fill sprite with black
    hr.setTextColor(TFT_ORANGE); // Orange text, no background
    hr.setTextDatum(MC_DATUM);   // Bottom right coordinate datum
    hr.setTextFont(1);
    hr.drawString("HR", 0, 0);
    // spo2 headline
    spo2.setColorDepth(8);
    spo2.createSprite(32, 8);
    spo2.fillSprite(TFT_BLACK);  // Fill sprite with black
    spo2.setTextColor(TFT_BLUE); // White text, no background
    spo2.setTextDatum(MC_DATUM); // Bottom right coordinate datum
    spo2.setTextFont(1);
    spo2.drawString("SPO2", 0, 0);
    // hr value position and color
    stext1.setColorDepth(8);
    stext1.createSprite(32, 32);
    stext1.fillSprite(TFT_BLACK);    // Fill sprite with black
    stext1.setTextColor(TFT_ORANGE); // White text, no background
    stext1.setTextDatum(MC_DATUM);   // Bottom right coordinate datum
    stext1.setTextFont(4);
    stext1.drawString("00", 0, 0);
    // spo2 value
    spo2text.setColorDepth(8);
    spo2text.createSprite(32, 32);
    spo2text.fillSprite(TFT_BLACK);  // Fill sprite with black
    spo2text.setTextColor(TFT_BLUE); // White text, no background
    spo2text.setTextDatum(MC_DATUM); // Bottom right coordinate datum
    spo2text.setTextFont(4);
    spo2text.drawString("00", 0, 0);
    // mesh connection information
    stext2.createSprite(148, 8);
    stext2.fillSprite(TFT_BLACK);
    stext2.setTextColor(TFT_WHITE); // White text, no background
    stext2.setTextDatum(MC_DATUM);  // Bottom right coordinate datum
    stext2.setTextFont(1);
    // patient name 
    patientname.createSprite(148, 8);
    patientname.fillSprite(TFT_BLACK);
    patientname.setTextColor(TFT_WHITE); // White text, no background
    patientname.setTextDatum(MC_DATUM);  // Bottom right coordinate datum
    patientname.setTextFont(1);
    patientname.drawString(String(CALLBACK), 0, 0);

    tft.fillScreen(TFT_BLACK);
    spr.createSprite(64, 64);
    spr.setSwapBytes(false);
    spr.pushImage(0, 0, 64, 64, heart);

    //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | MSG_TYPES | REMOTE ); // all types on except GENERAL
    if (DEBUG)
    {
        mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE); // all types on
    }
    else
    {
        mesh.setDebugMsgTypes(ERROR | STARTUP); // set before init() so that you can see startup messages
    }

    mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6); // , WIFI_AP_STA, 6
    //mesh.init(MESH_PREFIX, MESH_PASSWORD, MESH_PORT);
    mesh.onReceive(&receivedCallback);
    mesh.onNewConnection(&newConnectionCallback);
    mesh.onChangedConnections(&changedConnectionCallback);
    mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
    // if you want your node to accept OTA firmware, simply include this line
    // with whatever role you want your hardware to be. For instance, aMessage
    mesh.initOTAReceive(ROLE);
    // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
    mesh.setContainsRoot(true);
    Serial.printf("My Nodeid: %u\n", mesh.getNodeId());
    sprintf(CONNECTED, "Id:%u", mesh.getNodeId());
    userScheduler.addTask(taskSendMessage);
    taskSendMessage.enable();

    spr.pushSprite(10, 1);
    showConnectionStatus();
    hr.pushSprite(75, 18);
    spo2.pushSprite(120, 18);
    showPatientName();
}


void loop()
{
    int y = 1;
    uint32_t updateTime = 0; // time for next update
    uint32_t sleppTimeOut = millis();
    bool rever = false;
    while (1)
    {
        // it will run the user scheduler as well
        mesh.update();
        loopMAX30105();

        if (updateTime <= millis())
        {
            updateTime = millis() + 30;
            if (pox.getHeartRate() > 40)
            {
                if (rever)
                {
                    y--;
                    if (y <= -10)
                    {
                        rever = false;
                    }
                }
                else
                {
                    y++;
                    if (y + 64 >= 80)
                    {
                        rever = true;
                    }
                }

                spr.pushSprite(10, y);
                sleppTimeOut = millis();
            }
        }
        if (millis() - sleppTimeOut > 10000) // no beat since 10 sec
        {
            onBeatDetected();
        }
        if (millis() - sleppTimeOut > SCREEN_TIMEOUT)
        {
            enterDeepsleep();
        }
    }
}

void enterDeepsleep()
{
    tft.fillScreen(TFT_BLACK);
    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.setTextDatum(MC_DATUM);
    tft.drawString("Press again to wake up", tft.width() / 2, tft.height() / 2);
    Serial.println("Go to Sleep");
    pox.shutdown();
    mesh.stop();
    delay(3000);
    tft.writecommand(ST7735_DISPOFF);
    tft.writecommand(ST7735_SLPIN);

    esp_sleep_enable_ext1_wakeup(GPIO_SEL_38, ESP_EXT1_WAKEUP_ALL_LOW);
    esp_deep_sleep_start();
}
